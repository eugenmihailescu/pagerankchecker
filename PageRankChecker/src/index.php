<?php
/**
 * ----------------------------------------------------------------------------
 * This file is part of PageRankChecker.
 * 
 * PageRankChecker is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * PageRankChecker is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * PageRankChecker. If not, see <http://www.gnu.org/licenses/>.
 * 
 * ----------------------------------------------------------------------------
 * 
 * Git revision information:
 * 
 * @version:	0.2.1-alfa-0-g97f584c $
 * @commit:		97f584cf2e14fc63bf2741a21973f64dd1d2c740 $
 * @author:		Eugen Mihailescu <eugenmihailescux@gmail.com> $
 * @date:		Sat Feb 1 01:11:29 2014 +0100 $
 * @file:		index.php $
 * 
 * @id:	index.php | Sat Feb 1 01:11:29 2014 +0100 | Eugen Mihailescu  $
 * 
 */
?>

<head>
<link rel="stylesheet" type="text/css" href="css/style.css">
</head>

<body onload="process_choice(this,document.checkpr.proxy);process_choice(this,document.checkpr.proxy_vidalia);">
<script src="js/scripts.js"></script>
<?php

set_time_limit(0);// it can run as long as it wishes
include('pagerank-functions.php');

session_start();

global $urlList, $sitemap, $strip_text, $proxy_type, $proxy, $proxy_rand, $proxy_vidalia, $sleep, $engine, $timeout, $partial, $retry, $hiderr, $noimg;

/**
 * Output table row color and row link style
 */
$pn_color = array("green", "yellow", "orange", "red", "black");//color by page_no (1=green,2=yellow,..)
$lk_color = array('', 'class="a-yellow"', '', '', '');//link style by page_no (1=default,2=a-yellow,3=default,..)

/**
 * Load a comma-delimited list of proxy,port,type
 * 
 * @param string $filename
 * @return array of array(proxy,port,type)
 */
function getProxies($filename) {
	$result = array();
	$csv_list = file_get_contents($filename);
	$records = explode("\n", $csv_list);
	foreach ($records as $proxy) {
		$proxy_info = explode(",", $proxy);
		if (count($proxy_info) == 3) {
			$item = array($proxy_info[0], $proxy_info[1], $proxy_info[2]);
			$result[count($result)] = $item;
		}
	}
	return $result;
}

/**
 * Load a text file of proxy:port
 * 
 * @param string $filename
 * @return array of proxy:port items 
 */
function getProxies1($filename) {
	$result = array();
	$txt_list = file_get_contents($filename);
	$records = explode("\n", $txt_list);
	foreach ($records as $proxy) {
		$result[count($result)] = $proxy;
	}
	return $result;
}
/**
 * Populates $urlList with all the links found within the XML sitemaps
 * @param resource $parser
 * @param string $data
 */
function handler($parser, $data) {
	global $urlList;
	if (substr($data, 0, 4) == "http") {
		array_push($urlList, $data);
	}
}
/**
 * Download the XML sitemap
 * @param string $sitemap
 * @throws Exception
 * @return the file content
 */
function downloadSitemap($sitemap) {
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_URL, $sitemap);
	$data = curl_exec($ch);
	if (curl_errno($ch) != 0) {
		$err = curl_error($ch);
		$errno = curl_errno($ch);
		echo_output(
				'<span class="error">' . $err . ' (code: ' . $errno
						. ').<br>Check your sitemap file : "' . $sitemap
						. '"</span>');
	}
	curl_close($ch);
	return $data;
}
/**
 * Calculates the traffic share as function of position
 * @param int $x the position of keyword/website on search engine
 * @return float the percentage of traffic share the website will probably get
 * @see http://chitika.com/google-positioning-value
 */
function traffic_share($x) {
	if ($x < 16)// this function estimates only when position (x) is upto 15
		$result = 1.084451323 * pow(10, -4) * pow($x, 6)
				- 6.031475741 * pow(10, -3) * pow($x, 5)
				+ 1.357173995 * pow(10, -1) * pow($x, 4)
				- 1.583293441 * pow($x, 3) + 10.18556257 * pow($x, 2)
				- 35.50005587 * pow($x, 1) + 59.08923075;
	else
		$result = 0;
	return number_format($result, 1);
}
/**
 * This is a callback function used to process the $searchString 
 * just before it is sent to the search engine.
 * Here I used it to strip some prefix/suffix from the searchString.
 * 
 * @param string $searchString
 * @return string 
 */
function stripper($searchString) {
	global $strip_text;// this is the parameter sent by the user in form
	return preg_replace('/' . $strip_text . '/', '', $searchString);
}
/**
 * Set the session variable by POST values and also 
 * initialize the correponding global variable
 * 
 * @param string $var_name the name of the POST value
 * @param resource $variable the address of the global variable
 * @param string $default_value the default value to be used 
 * when the POST value isn't defined
 */
function setSessionVar($var_name, &$variable, $default_value = '') {
	if (isset($_POST[$var_name]) && $_POST[$var_name] != '') {
		$variable = $_POST[$var_name];
	} else
		$variable = $default_value;
	$_SESSION[$var_name] = $variable;
}

// read/set the session parameters (see form.php)
setSessionVar('sitemap', $sitemap);
setSessionVar('strip_text', $strip_text);
setSessionVar('proxy', $proxy);
setSessionVar('proxy_type', $proxy_type, '-1');
setSessionVar('proxy_rand', $proxy_rand, false);
setSessionVar('proxy_vidalia', $proxy_vidalia);
setSessionVar('sleep', $sleep, 1);
setSessionVar('engine', $engine, 'www.google.com');
setSessionVar('partial', $partial, false);
setSessionVar('timeout', $timeout, 10);
setSessionVar('retry', $retry, false);
setSessionVar('hiderr', $hiderr, false);
setSessionVar('noimg', $noimg, false);

echo_output("<h1 class='title'>Check the pagerank of your entire website</h1>");
echo_output(
		'<h3 class="title">Powered by <a class="a-yellow" href="https://bitbucket.org/eugenmihailescu/pagerankchecker">PageRankChecker</a> v'
				. $VERSION . '</h3>');
if ($sitemap != '') {
	ob_start();
	echo_output(
			'<div class="title"><span class="error">Downloading your sitemap...');

	$data = downloadSitemap($sitemap);
	if ($data != "")
		echo_output("successfully");
	$urlList = array();
	$xml_parser = xml_parser_create();
	xml_set_character_data_handler($xml_parser, "handler");
	if (isset($data) && !xml_parse($xml_parser, $data)) {
		die(
				"Error parsing XML on line "
						. xml_get_current_line_number($xml_parser));
	} else if ($data != "")
		echo_output(" (" . count($urlList) . " nodes)");
	echo_output("</span></div>");
	xml_parser_free($xml_parser);
}

/**
 * print-out the form
 */
include_once 'form.php';

if (count($urlList) > 0) {
	echo_output(
			'<table class="result"><tr class="table-header"><td class="table-header-centered-cell">Node</td><td class="table-header-centered-cell">Page URL</td><td class="table-header-centered-cell">Page#</td><td class="table-header-centered-cell">Position#</td><td class="table-header-centered-cell">Traffic share</td></tr>');
	$start = new DateTime(date('Y-m-d H:i:s'));
}
if ($proxy_rand && $proxy_vidalia != '')
	$anonimously = $proxy_vidalia;
else
	$anonimously = false;

for ($i = 0; $i < count($urlList); $i++) {
	if ($noimg == true
			&& (strpos($urlList[$i], ".jpg") || strpos($urlList[$i], ".png")
					|| strpos($urlList[$i], ".gif")))
		continue;
	$done = false;
	while (!$done) {
		try {
			$link = '';
			$prObject = getKeywordPageRank(null, $urlList[$i], $engine, $proxy,
					$proxy_type, $anonimously, 'stripper', $timeout, $partial);
			$pr = $prObject->getPosition();
			$pn = $prObject->getPage();
			if ($pn >= 1 && ceil($pr / 3) <= count($pn_color)) {
				$color = $pn_color[ceil($pr / 3) - 1];
				$link = $lk_color[ceil($pr / 3) - 1];
			} else {
				$color = $pn_color[count($pn_color) - 1];
			}
			if ($pr > 0)
				$traffic = traffic_share($pr);
			else
				$traffic = 0;
			echo_output(
					'<tr><td></td><td colspan=4 class="keyword"><strong>Keyword</strong> : '
							. $prObject->getKeyword() . " ("
							. $prObject->getIp() . ")</td><tr>");
			echo_output(
					'<tr><td class="cnt">' . ($i + 1) . '</td><td class="'
							. $color . '-table-left-cell"><a ' . $link
							. ' href="' . $urlList[$i] . '">' . $urlList[$i]
							. '</a></td><td class="' . $color
							. '-table-centered-cell">' . $pn
							. '</td><td class="' . $color
							. '-table-centered-cell">' . $pr
							. '</td><td class="' . $color
							. '-table-centered-cell">' . $traffic
							. '%</td></tr>');
			$done = true;
		} catch (CurlException $e) {
			if (!$retry)
				$done = true;
			if (!$hiderr) {
				if (isset($prObject)) {
					$ip = $prObject->getIp();
					$tmp = $prObject->getTmpFile();
					if (!$done) {
						unlink($tmp);
						$tmp = '';
					}
				} else {
					$ip = $_SERVER['SERVER_ADDR'];
					$tmp = '';
				}
				echo_output(
						'<tr><td></td><td class="keyword" colspan=4><strong>Error</strong> : '
								. $e->getKeyword() . " (" . $ip . ")</td></tr>");
				echo_output(
						'<tr><td class="cnt">' . ($i + 1)
								. '</td><td class="red-table-left-cell" ><a '
								. $link . ' href="' . $e->getUrl() . '">'
								. $e->getUrl()
								. '</a></td><td class="red-table-centered-cell"><a href="'
								. $tmp . '">err[' . $e->getCode()
								. ']</a></td><td class="red-table-centered-cell" colspan=2>'
								. $e->getMessage() . '</td></tr>');
			}
		} catch (Exception $e) {
			if (!$retry)
				$done = true;
			if (!$hiderr) {
				echo_output(
						'<tr><td></td><td class="keyword" colspan=4><strong>Error</strong> : '
								. $e->getMessage() . "</td><tr>");
				echo_output(
						'<tr><td class="cnt">' . ($i + 1)
								. '</td><td class="red-table-left-cell"><a '
								. $link . ' href="' . $urlList[$i] . '">'
								. $urlList[$i]
								. '</a></td><td class="red-table-centered-cell">err['
								. $e->getCode()
								. ']</td><td class="red-table-centered-cell" colspan=2></td></tr>');
			}
			// when search engine not implemented (err=-30) just break the whole loop!
			if ($e->getCode() == -30) {
				$done = true;
				$i = count($urlList);
			}
		}
		sleep($sleep);
	}
}
echo_output('</table>');
if (count($urlList) > 0) {
	$end = new DateTime(date('Y-m-d H:i:s'));
	$elapsed_time = $start->diff($end);
	echo_output(
			'<div class="title"><span class="error">Job finished. Total elapsed time '
					. $elapsed_time->format('%H:%I:%S') . '</span></div>');
}
session_unset();
?>
</body>
