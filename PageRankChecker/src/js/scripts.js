/**
 * ----------------------------------------------------------------------------
 * This file is part of PageRankChecker.
 * 
 * PageRankChecker is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 * 
 * PageRankChecker is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * PageRankChecker. If not, see <http://www.gnu.org/licenses/>.
 * 
 * ----------------------------------------------------------------------------
 * 
 * Git revision information:
 * 
 * @version:	0.2.1-alfa-0-g97f584c $
 * @commit:		819d3ee82127c1fb9fd8deff56771b2cd96690d0 $
 * @author:		Eugen Mihailescu <eugenmihailescux@gmail.com> $
 * @date:		Fri Jan 31 22:26:06 2014 +0100 $
 * @file:		scripts.js $
 * 
 * @id:	scripts.js | Fri Jan 31 22:26:06 2014 +0100 | Eugen Mihailescu  $
 * 
 */

<!-- this will make the proxy selector to show/hide the proxy textbox
function activate(field) {
  field.disabled=false;
  if(document.styleSheets)field.style.visibility  = 'visible';
  field.focus();
}
  
function process_choice(selection,textfield) {
	
  if((selection.className=='button' && selection.selectedIndex>0)||(selection.className='chk_vidalia' && selection.checked)) {
    activate(textfield); }
  else {
    textfield.disabled = true;    
    if(document.styleSheets)textfield.style.visibility  = 'hidden';
     }
}
    
function valid(proxy_type,txt) {
  if(txt.value == '') {
    if(proxy_type.selectedIndex>0) {
      alert('You need to specify your proxy:port into the text box');
      return false; }
    else {
      return true; }}
  else {
    if(proxy_type.selectedIndex==0) {
      return true; }
    else {
      return true; }}
}
      
function check_choice() {
  if(!document.checkpr.proxy_type.selectedIndex>0) {
    document.checkpr.proxy.blur();
    alert('Please check your proxy type selection first');
    document.checkpr.proxy_type.focus(); }
}
