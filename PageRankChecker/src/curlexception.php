<?php

/**
 * ----------------------------------------------------------------------------
 * This file is part of PageRankChecker.
 * 
 * PageRankChecker is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * PageRankChecker is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * PageRankChecker. If not, see <http://www.gnu.org/licenses/>.
 * 
 * ----------------------------------------------------------------------------
 * 
 * Git revision information:
 * 
 * @version:	0.2.1-alfa-0-g97f584c $
 * @commit:		819d3ee82127c1fb9fd8deff56771b2cd96690d0 $
 * @author:		Eugen Mihailescu <eugenmihailescux@gmail.com> $
 * @date:		Fri Jan 31 22:26:06 2014 +0100 $
 * @file:		curlexception.php $
 * 
 * @id:	curlexception.php | Fri Jan 31 22:26:06 2014 +0100 | Eugen Mihailescu  $
 * 
 */
?>

<?php
/**
 * @author "Eugen Mihailescu"
 *
 */
class CurlException extends Exception {
	private $url;
	private $keyword;
	private $engine;

	private function setUrl($url) {
		$this->url = $url;
	}
	private function setKeyword($keyword) {
		$this->keyword = $keyword;
	}
	private function setEngine($engine) {
		$this->engine = $engine;
	}
	public function getUrl() {
		return $this->url;
	}
	public function getKeyword() {
		return $this->keyword;
	}
	public function getEngine() {
		return $this->engine;
	}

	public function __construct($message, $code = 0, Exception $previous = null,
			$url = null, $keyword = null, $engine = null) {

		parent::__construct($message, $code, $previous);
		$this->setUrl($url);
		$this->setKeyword($keyword);
		$this->setEngine($engine);
	}

}
?>
