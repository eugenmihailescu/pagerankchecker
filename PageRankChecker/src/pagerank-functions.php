<?php
/**
 * ----------------------------------------------------------------------------
 * This file is part of PageRankChecker.
 * 
 * PageRankChecker is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * PageRankChecker is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * PageRankChecker. If not, see <http://www.gnu.org/licenses/>.
 * 
 * ----------------------------------------------------------------------------
 * 
 * Git revision information:
 * 
 * @version:	0.2.1-alfa-0-g97f584c $
 * @commit:		97f584cf2e14fc63bf2741a21973f64dd1d2c740 $
 * @author:		Eugen Mihailescu <eugenmihailescux@gmail.com> $
 * @date:		Sat Feb 1 01:11:29 2014 +0100 $
 * @file:		pagerank-functions.php $
 * 
 * @id:	pagerank-functions.php | Sat Feb 1 01:11:29 2014 +0100 | Eugen Mihailescu  $
 * 
 */
?>

<?php
include_once 'pagerank.php';
include_once 'curlexception.php';
include_once 'constants.php';

/**
 * Extracts the last two segments of host name (eg. <google>.<com>)
 * @param string $url
 * @return string|boolean if found then it returns the last two segments of domain name else returns boolean false
 */
function extractDomainName($url) {
	if (preg_match('/[^.]+\.[^.]+$/', $url, $matches) == 1)
		return $matches[0];
	else
		return false;
}
/**
 * Check if the input string contains an Ip address substring 
 * @param string $str
 * @return boolean true if an valid IP address found, false otherwise
 */
function hasIpAddress($str) {
	return (preg_match('/\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}/', $str, $match)
			&& filter_var($match[0], FILTER_VALIDATE_IP));
}
/**
 * Output the stripped $str to page/console
 * @param string $str the string to be outputed
 * @param bool $is_console true if the $str will be stripped
 */
function echo_output($str, $is_console = false) {
	if ($is_console)
		echo strip_tags(str_replace("<br>", "\n\r", $str));
	else
		echo $str;
	flush();
	@ob_end_flush();
}

/**
 * Return current timestamp as float 
 * @return number
 */
function microtime_float() {
	list($usec, $sec) = explode(" ", microtime());
	return ((float) $usec + (float) $sec);
}

/**
 * This test a proxy server and returns the test status
 * @param string $host the proxy host address
 * @param int $port the proxy port
 * @param int $type the proxy CURL type
 * @return array of error code, status message, speed bytes/sec and duration in microseconds 
 */
function testProxy($host, $port, $type) {
	$time_start = microtime_float();

	$ch = getCurlObject('http://info.cern.ch/hypertext/WWW/TheProject.html',
			$host . ":" . $port, $type);
	$html = curl_exec($ch);
	$time_end = microtime_float();
	$elapsed_time = $time_end - $time_start;
	if (strpos($html, '<TITLE>The World Wide Web project</TITLE>')) {
		$speed = 1e6 * strlen($html) / $elapsed_time;
		$err = 0;
		$status = "success";
	} else {
		$speed = 0;
		$status = "does not return the required content";
		$err = -1;
	}
	if (curl_errno($ch) != 0) {
		$err = curl_errno($ch);
		$status = curl_error($ch);
	}
	curl_close($ch);
	return array($err, $status, $speed, $elapsed_time);
}

/**
 * Send a signal to Tor/Vidalia to  switch between nodes to get a new IP
 * 
 * @param string $anonimously the address:port for Vidalia proxy
 * @param int the number of seconds after which the connection is regarded as not functional
 * @throws Exception
 */
function switchIp($anonimously, $timeout = 10) {
	// connect to Vidalia and ask it to get a new tor random IP
	$parts = explode(":", $anonimously);
	if (count($parts == 2)) {
		$ip = $parts[0];//Vidalia address
		$port = $parts[1];//Vidalia port
	}

	if ($fp = @fsockopen($ip, $port, $error_number, $err_string, $timeout)) {
		fwrite($fp, "AUTHENTICATE \"PASSWORD\"\n");
		$received = fread($fp, 512);
		fwrite($fp, "signal NEWNYM\n");
		$received = fread($fp, 512);
	} else
		throw new Exception("Error connecting $ip:$port : $err_string",
				$error_number);

	fclose($fp);
}

function followUrl($url) {
	$p = strpos($url, "**");
	if ($p > 0) {
		$result = htmlspecialchars_decode(
				substr($url, $p + 2, strlen($url) - $p - 1), ENT_QUOTES);
		$result = preg_replace("/%u([0-9a-f]{3,4})/i", "&#x\\1;",
				urldecode($result));
		$result = html_entity_decode($result, null, 'UTF-8');
	} else
		$result = $url;
	return $result;
}
/**
 * Creates and initiates a Curl object. It also uses fake user agent
 * and HTTP headers to trick the target webserver and disguises itself
 * 
 * @param string $url
 * @param string $proxy address:port
 * @param string $proxy_type a CURLPROXY_ valid constant
 * @param int timeout the number of seconds to wait before the unresponsive connection/query is timeout
 * @return Curl object 
 */
function getCurlObject($url, $proxy = null, $proxy_type = null, $timeout = 10) {
	global $userAgent_array;
	//Randomly select a user agent from the user agent array
	$userAgent = $userAgent_array[rand(0, count($userAgent_array) - 1)];

	$ch = curl_init();
	// disguises the curl using fake headers and a fake user agent
	$header[0] = "Accept: text/xml,application/xml,application/xhtml+xml,";
	$header[0] .= "text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5";
	$header[] = "Cache-Control: max-age=0";
	$header[] = "Connection: keep-alive";
	$header[] = "Keep-Alive: 300";
	$header[] = "Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7";
	$header[] = "Accept-Language: en-us,en;sv,sv-se;q=0.5";
	$header[] = "Pragma: ";
	curl_setopt($ch, CURLOPT_URL, $url);
	if (isset($proxy)) {
		curl_setopt($ch, CURLOPT_PROXY, $proxy);//tor socks5
		if (isset($proxy_type))
			curl_setopt($ch, CURLOPT_PROXYTYPE, $proxy_type);
	}
	// @see http://curl.haxx.se/libcurl/c/curl_easy_setopt.html
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_VERBOSE, 0);
	curl_setopt($ch, CURLOPT_USERAGENT, $userAgent);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
	curl_setopt($ch, CURLOPT_REFERER, 'http://www.wikipedia.org');
	curl_setopt($ch, CURLOPT_ENCODING, 'gzip,deflate');
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
	curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
	curl_setopt($ch, CURLOPT_TIMEOUT, $timeout); // make sure to set this otherwise we might wait indefinitely; you can override this,though
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);// make sure to set this otherwise we might wait indefinitely; you can override this,though
	return $ch;
}

/**
 * First tries to read the page title (the OpenGraph metatag) and 
 * if not found then tries to get it from the HTML page title metatag.
 * 
 * @param string $url
 * @throws Exception
 * @return string
 */
function getPageTitle($url, $proxy, $proxy_type) {

	$ch = getCurlObject($url, $proxy, $proxy_type);
	$html = curl_exec($ch);

	if (curl_errno($ch) != 0) {
		$err = curl_error($ch);
		$errno = curl_errno($ch);
		curl_close($ch);
		throw new Exception($err, $errno);
	}
	curl_close($ch);

	$doc = new DOMDocument();
	@$doc->loadHTML($html);

	foreach ($doc->getElementsByTagName('meta') as $meta) {
		$prop = $meta->getAttribute('property');
		if (isset($prop) && $prop == "og:title") {
			$result = $meta->getAttribute('content');
			break;
		}
	}
	if (!isset($result) || $result == "") {
		$tag = $doc->getElementsByTagName('title');
		if ($tag->length == 0)
			$result = "";
		else
			$result = $doc->getElementsByTagName('title')->item(0)->textContent;
	}
	return $result;
}

/**
 * Retrieve the effective URL.
 * It could be redirected, right?
 * 
 * @param string $the_url
 * @return string
 */
function getUrl($url) {
	$ch = curl_init($url);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_exec($ch);
	$url = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
	curl_close($ch);
	return $url;
}

/**
 * Query the search $engine for $searchString and returns an object
 * that tells the page rank and position of your $urlAddress
 * 
 * @param string $seachString
 * @param string $urlAddress
 * @param string $engine like %.google.% or %.bing.% or search.yahoo.com
 * @param string $proxy address:port
 * @param int $proxy_type a CURLPROXY_ valid constant
 * @param mixt $anonimously address:port if you want to use Vidalia which ask for new IP from your TOR proxy (if that is what you're using), false otherwise
 * @param int timeout the number of seconds to wait before the unresponsive connection/query is timeout
 * @param bool true when you want that if at least the $urlAddress domain is found then the $seachString is regarded ranked
 * @throws Exception
 * @throws CurlException
 * @return a PageRank object with info about keyword ranking
 */
function getKeywordPageRank($seachString, $urlAddress, $engine, $proxy,
		$proxy_type, $anonimously, $handler = null, $timeout = 10,
		$partial = true) {
	global $RESULTS_ON_PAGE;
	global $DEFAULT_SEARCH_ENGINE;

	// if no searchString provided then assume the page title as the working keyword
	if (!isset($seachString) || $seachString == "") {
		if ($anonimously != '')
			switchIp($anonimously, $timeout);

		$seachString = getPageTitle($urlAddress, $proxy, $proxy_type);
	}

	// Call your custom handler that has the chance to alter
	// somehow the searchString before using it as is
	if (is_callable($handler))
		$seachString = call_user_func($handler, $seachString);

	if (isset($seachString) && $seachString != "")
		$seachStringword = urlencode(
				htmlspecialchars_decode($seachString, ENT_QUOTES));
	else
		$seachStringword = "";
	if ($seachStringword == "")
		throw new Exception("The page has no title, no keyword found.", -22);

	$file = tempnam(sys_get_temp_dir(), "pagerank_");

	// convert urlAddress to html chars
	$urlAddress = htmlspecialchars_decode($urlAddress, ENT_QUOTES);
	$old_urlAddress = $urlAddress;

	// if no engine provided assume google.com as default
	if (isset($engine) && $engine != '')
		$searchEngine = $engine;
	else
		$searchEngine = $DEFAULT_SEARCH_ENGINE;

	// Note that although $RESULTS_ON_PAGE might be 10 I am searching 
	// for $RESULTS_ON_PAGE*10 which means the firts 10 pages !
	if (strpos($searchEngine, ".yahoo."))
		$searchStringUrl = $searchEngine . '/search?p=' . $seachStringword
				. '&n=' . $RESULTS_ON_PAGE . '0';
	else
		$searchStringUrl = $searchEngine . '/search?q=' . $seachStringword
				. '&num=' . $RESULTS_ON_PAGE . '0&pws=0';
	// by default we start from page 1, position 1
	$position = 1;
	$pageNo = 1;
	if ($anonimously != '')
		switchIp($anonimously, $timeout);

	// get proxy's IP address
	$ch = getCurlObject('http://checkip.dyndns.com/', $proxy, $proxy_type);
	$html = curl_exec($ch);
	preg_match('/Current IP Address: ([\[\]:.[0-9a-fA-F]+)</', $html, $m);
	if (count($m) > 0)
		$ip = $m[1];
	else
		$ip = $_SERVER['SERVER_ADDR'];

	// read the content of $searchStringUrl
	curl_setopt($ch, CURLOPT_URL, $searchStringUrl);
	$html = curl_exec($ch);
	file_put_contents($file, $html);

	// if Google bans our IP then throw an exception
	if (strpos($html,
			'Our systems have detected unusual traffic from your computer network')
			|| strpos($html,
					'your computer or network may be sending automated queries')) {
		curl_close($ch);
		throw new CurlException(
				"Sorry but " . $searchEngine . " banned this IP " . $ip
						. " network!", -10, null, $urlAddress,
				urldecode($seachStringword), $searchStringUrl);
	}

	// in case of any other error throw an exception
	if (curl_errno($ch) != 0) {
		$err = curl_error($ch);
		$errno = curl_errno($ch);
		curl_close($ch);
		throw new CurlException($err, $errno, null, $urlAddress,
				urldecode($seachStringword), $searchStringUrl);
	}
	curl_close($ch);

	// parse the readed Html document
	$dom = new DOMDocument();
	@$dom->loadHTML($html);
	$xpath = new DOMXPath($dom);

	// when Google then we expect the following structure:
	// html->...->div[id=ires]->ol->li->h3->a
	if (strpos($searchStringUrl, ".google."))
		$urlNodes = $xpath
				->query(
						'/html//div[@id="ires"]/ol/li//h3[not(ancestor::span)]/a');
	else 
	// when Bing then we expect one of the two structures below:
	// html->...->div[id=results|b_content]->ul|ol->li->h3->a
	if (strpos($searchStringUrl, ".bing.")) {
		$urlNodes = $xpath
				->query(
						'/html//div[@id="results"]/ul/li//h3[not(ancestor::span)]/a');
		if ($urlNodes->length == 0)
			$urlNodes = $xpath
					->query(
							'/html//div[@id="b_content"]/ol/li//h2[not(ancestor::span)]/a');
	} else if (strpos($searchStringUrl, ".yahoo.")) {
		$urlNodes = $xpath->query('/html//div[@id="web"]/ol/li//div/h3/a');
	} else
		throw new Exception(
				"This search engine ($searchStringUrl) is not implemented.",
				-30);

	// create and init page rank object
	$result = new PageRank();
	$result->setUrl(htmlspecialchars($urlAddress, ENT_QUOTES));
	$result
			->setKeyword(
					htmlspecialchars(urldecode($seachStringword), ENT_QUOTES));
	$result->setEngine($searchEngine);
	$result->setDate(date('Y-m-d'));
	$result->setIp($ip);
	$result->setTmpFile($file);
	$result->setPage(-1);
	$result->setPosition(-1);
	$parseUrlAddress = parse_url($urlAddress);
	$host = explode('.', $parseUrlAddress['host']);
	$host = array_slice($host, count($host) - 2);

	if (isset($parseUrlAddress['host']))
		$pHost = $parseUrlAddress['host'];
	else
		$pHost = '';
	if (isset($parseUrlAddress['path']))
		$pPath = $parseUrlAddress['path'];
	else
		$pPath = '';
	$pHostPath = $pHost . $pPath;
	$hostPathLen = strlen($pHostPath);

	$result_found = false;
	if (!is_null($urlNodes))
		foreach ($urlNodes as $node) {
			$url = $node->getAttribute('href');
			if (strpos($searchEngine, ".yahoo."))
				$url = followUrl($url, $proxy, $proxy_type);
			@$kw_Host = parse_url($url, PHP_URL_HOST);
			@$kw_HostPath = $kw_Host . parse_url($url, PHP_URL_PATH);
			if ($partial) {
				if (isset($pHost) && $pHost != '')
					$p_domain = extractDomainName($pHost);
				if (isset($kw_Host) && $kw_Host != '')
					$kw_domain = extractDomainName($kw_Host);
			}
			if (($pHostPath == substr($kw_HostPath, 0, $hostPathLen))
					|| (stristr($kw_HostPath, '.' . $pHostPath))
					|| ($partial && isset($p_domain) && isset($kw_domain)
							&& $p_domain == $kw_domain)) {
				$result->SetUrl(htmlspecialchars($url, ENT_QUOTES));
				$result->SetPosition($position);
				$result->setPage(ceil($position / $RESULTS_ON_PAGE));
				$result_found = TRUE;
				break;// we found our result, that's it, parse no more!
			}
			$position++;
		}
	if (!$result_found) {
		$result->SetUrl(htmlspecialchars(getUrl($urlAddress), ENT_QUOTES));
	}
	$result->setTmpFile(null);
	unlink($file);
	return $result;
}

?>
