<?php
/**
 * ----------------------------------------------------------------------------
 * This file is part of PageRankChecker.
 * 
 * PageRankChecker is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * PageRankChecker is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * PageRankChecker. If not, see <http://www.gnu.org/licenses/>.
 * 
 * ----------------------------------------------------------------------------
 * 
 * Git revision information:
 * 
 * @version:	0.2.1-alfa-0-g97f584c $
 * @commit:		819d3ee82127c1fb9fd8deff56771b2cd96690d0 $
 * @author:		Eugen Mihailescu <eugenmihailescux@gmail.com> $
 * @date:		Fri Jan 31 22:26:06 2014 +0100 $
 * @file:		form.php $
 * 
 * @id:	form.php | Fri Jan 31 22:26:06 2014 +0100 | Eugen Mihailescu  $
 * 
 */
?>

<form method="post" name="checkpr" id="checkpr" onsubmit="return valid(this.proxy_type,this.proxy)">
<table class="form">
<tr>
<td>sitemap.xml URL :</td><td colspan=4><input type="text" name="sitemap" title='Enter the full URL to your XML sitemap' value="<?php echo $sitemap; ?>" size=70 /></td>
</tr>
<tr><td><a class="a-yellow" target="_blank" href="http://www.regexr.com/">Strip'n keyword:</a></td><td colspan=4><input type="text" name="strip_text" title='A regex pattern that if matches then it will be stripped from the keyword before it is begin used' value="<?php echo $strip_text; ?>" size=70/></td></tr>
<tr><td>Proxy :</td>
<td>
<table class="inner-table"><tr><td>
<select class="button" name="proxy_type" title='If you want to use a proxy (even a SOCKS proxy like TOR) select the proxy type then specify its address:port' onchange="process_choice(this,document.checkpr.proxy)">
<option value="-1" <?php if (isset($proxy_type) && $proxy_type == "-1")
	echo "selected";
				   ?>>no proxy</option>
<option value="<?php echo CURLPROXY_HTTP; ?>" <?php if (isset($proxy_type)
		&& $proxy_type == CURLPROXY_HTTP)
	echo "selected";
											  ?>>http</option>
<option value="<?php echo CURLPROXY_SOCKS4; ?>" <?php if (isset($proxy_type)
		&& $proxy_type == CURLPROXY_SOCKS4)
	echo "selected";
												?>>socks4</option>
<option value="<?php echo CURLPROXY_SOCKS5; ?>" <?php if (isset($proxy_type)
		&& $proxy_type == CURLPROXY_SOCKS5)
	echo "selected";
												?>>socks5</option>
</select>
<input type="text" name="proxy" title='Enter your proxy address:port' value="<?php echo $proxy; ?>" <?php if ($proxy_type
		== -1)
	echo ' style="visibility:hidden"'
																									?>
></td>
<td colspan=3><input class="chk_vidalia" type="checkbox" name="proxy_rand" title='Checked when you want to use Vidalia to switch between different proxy nodes' value="true" <?php if ($proxy_rand)
	echo "checked";
																																											 ?> onchange="process_choice(this,document.checkpr.proxy_vidalia)">Randimize IP
<input type="text" name="proxy_vidalia" title='Enter your Vidalia address:port' value="<?php echo $proxy_vidalia; ?>" <?php if (!$proxy_rand)
	echo ' style="visibility:hidden"'
																													  ?>
></td></tr></table></td></tr>

<tr><td>Engine :</td><td>
<table class="inner-table"><tr><td><input type="text" name='engine' title='Enter your search engine address (eg. www.google.com)' value="<?php echo $engine; ?>" size=20/></td>
<td colspan=2>Sleep :<input type="text" name='sleep' title='Enter how many seconds to wait between two consecutive queries' value="<?php echo $sleep; ?>" size=2/></td>
<td><input type="checkbox" name="partial" title='Check this if you want that when at least the domain anme is found we regard this as a match' value="true" <?php if ($partial
		== 'true')
	echo "checked";
																																							?>/>Partial match</td></tr></table>
</td>
</tr>
<tr><td>Timeout:</td><td>
<table class="inner-table"><tr><td>
<input type="text" name="timeout" title="Enter the number of seconds to wait before to abort waiting for the connection' response" value="<?php echo $timeout; ?>" size=3/> sec</td>
<td><input type="checkbox" name='retry' title="If using Vidalia+TOR and Google says 'your IP is banned' then try another IP" value="true" <?php if ($retry
		== 'true')
	echo "checked";
																																		  ?>/>Retry on banned IP												 
</td>
<td>
<input type="checkbox" name='hiderr' title='Hide error messages from output' value="true" <?php if ($hiderr
		== 'true')
	echo "checked";
																						  ?>/>Hide errors<br>
</td>
<td>
																						  
<input type="checkbox" name='noimg' title='If your sitemap contains images then do not check their page rank (as no keyword exists)' value="true" <?php if ($noimg
		== 'true')
	echo "checked";
																																				  ?>/>Ignore image URLs
</td></tr></table></td></tr>
<tr><td colspan=5>
																																				  
<input type="submit" value="Process" title='Put the whole thing at work!' class="button"/></td></tr>
</table>
</form>
