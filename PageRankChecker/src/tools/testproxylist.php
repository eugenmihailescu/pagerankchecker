<?php
/**
 * ----------------------------------------------------------------------------
 * This file is part of PageRankChecker.
 * 
 * PageRankChecker is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * PageRankChecker is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * PageRankChecker. If not, see <http://www.gnu.org/licenses/>.
 * 
 * ----------------------------------------------------------------------------
 * 
 * Git revision information:
 * 
 * @version:	0.2.1-alfa-0-g97f584c $
 * @commit:		819d3ee82127c1fb9fd8deff56771b2cd96690d0 $
 * @author:		Eugen Mihailescu <eugenmihailescux@gmail.com> $
 * @date:		Fri Jan 31 22:26:06 2014 +0100 $
 * @file:		testproxylist.php $
 * 
 * @id:	testproxylist.php | Fri Jan 31 22:26:06 2014 +0100 | Eugen Mihailescu  $
 * 
 */
?>

<?php
include_once '../pagerank-functions.php';
set_time_limit(0);// it can run as long as it wishes

// detects and handle the termination signals (like Ctrl+C, Ctrl+Z) 
global $ctrl_c;
$ctrl_c = false;
declare(ticks=1);
pcntl_signal(SIGINT, "signal_handler");
pcntl_signal(SIGTERM, "signal_handler");

function signal_handler($signal) {
	global $ctrl_c;
	$ctrl_c = $signal == SIGTERM || $signal == SIGINT || $signal == SIGKILL;
}

/**
 * To run it from OS console: php testproxylist.php -f../res/proxy_list.csv -c
 * where	f=input CSV filepath
 *			c=I'm running at console so please echo like console-friendly
 *
 */
$options = getopt("c::f::");
$is_console = isset($options['c']);
if (isset($options['f']))
	$input_file = $options['f'];
else if (isset($_GET['f'])) {
	$input_file = $_GET['f'];
}

/**
 * Returns the last n-lines of a text file
 * @param string $filepath
 * @param int $lines
 * @return boolean|string
 */
function getLastLine($filepath, $lines = 1) {

	$f = @fopen($filepath, "r");
	if ($f === false)
		return false;

	fseek($f, -1, SEEK_END);

	if (fread($f, 1) != "\n")
		$lines -= 1;

	$output = '';
	$chunk = '';

	while (ftell($f) > 0 && $lines >= 0) {
		$seek = min(ftell($f), 4096);
		fseek($f, -$seek, SEEK_CUR);
		$output = ($chunk = fread($f, $seek)) . $output;
		fseek($f, -mb_strlen($chunk, '8bit'), SEEK_CUR);
		$lines -= substr_count($chunk, "\n");
	}

	while ($lines++ < 0)
		$output = substr($output, strpos($output, "\n") + 1);

	fclose($f);
	return trim($output);
}

if (isset($input_file) && $input_file != '')
	if (file_exists($input_file)) {
		$err_file = $input_file . '.error';
		$ok_file = $input_file . '.success';

		$resume = file_exists($err_file) || file_exists($ok_file);

		// if the last session didn't finished successfully then resume
		if ($resume) {
			if (file_exists($err_file)) {
				$last_error = getLastLine($err_file);
				$last_err_line = explode(",", $last_error);
				$last_err_host = $last_err_line[0];
				$last_err_port = $last_err_line[1];
			}

			if (file_exists($ok_file)) {
				$last_ok = getLastLine($ok_file);
				$last_ok_line = explode(",", $last_ok);
				$last_ok_host = $last_ok_line[0];
				$last_ok_port = $last_ok_line[1];
			}
		}

		// process a CSV proxy list in order to test the proxies' speed 
		$csv = file_get_contents($input_file);
		$list = explode("\n", $csv);
		$result = array();
		$total = count($list);
		$i = 0;

		foreach ($list as $item) {
			//Ctrl+C pressed, exit!
			if ($ctrl_c)
				exit;

			$i++;

			$data = explode(",", $item);
			if (count($data) > 1) {
				$host = $data[0];
				$port = $data[1];

				// if we've not reached the last err/ok host:port as the one in the log
				if ($resume)
					if ((!isset($last_err_host)
							|| ($last_err_host != $host
									|| $last_err_port != $port))
							&& (!isset($last_ok_host)
									|| ($last_ok_host != $host
											|| $last_ok_port != $port)))
						continue;
					else {
						echo_output(
								"Attempting to resume the last session...<br>",
								$is_console);
						$resume = false;//done, no more resume
					}

				if (stripos($item, "socks4"))
					$type = CURLPROXY_SOCKS4;
				else if (stripos($item, "socks5"))
					$type = CURLPROXY_SOCKS5;
				else
					$type = CURLPROXY_HTTP;

				echo_output("[$i/$total] | $host:$port | ", $is_console);
				$test = testProxy($host, $port, $type);
				if ($test[0] == 0) {
					$status = $test[1];

					if ($test[3] / 1e6 < 1)
						$color = "green";
					else if ($test[3] / 1e6 < 4)
						$color = "yellow";
					else if ($test[3] / 1e6 < 10)
						$color = "orange";
					else
						$color = "red";
					$style = 'color:white;background-color:' . $color;
				} else {
					$status = "error ($test[1] ; code: $test[0])";
					$style = 'color:white;background-color:black';
				}

				$speed = ceil(1e3 * $test[3]);
				echo_output(
						'<span style="' . $style . '"> ' . $status
								. '</span> | ' . $speed . " ms<br>",
						$is_console);

				// log the progress into 2 external files (success|error) for resuming purpose
				if ($status == 'success') {
					$log_file = $ok_file;
					$data_ext = '';
					$result[count($result)] = array('host' => $host,
							'port' => $port,
							'type' => ($type == CURLPROXY_HTTP ? 'http'
									: $type == CURLPROXY_SOCKS4 ? 'socks4'
											: 'socks5'), 'speed' => $speed);
				} else {
					$log_file = $err_file;
					$data_ext = ' -> ' . $status;
				}
				$data = $host . "," . $port . "," . $type . "," . $speed
						. $data_ext . "\n";
				file_put_contents($log_file, $data, FILE_APPEND);

			}

		}
		if (!$ctrl_c) {
			if (file_exists($err_file))
				unlink($err_file);
			if (file_exists($ok_file))
				unlink($ok_file);
		}
	} else
		die("File $input_file doesn't exist!");

// if an output_file was specified then sort (by proxy's speed) 
// and save the the result in the specified output-file
if (!$ctrl_c) {
	if (isset($input_file)) {
		$result = array_unique($result);//remove duplicates, if any (due to Ctrl+C/resume)
		$hosts = array();
		$ports = array();
		$types = array();
		$speeds = array();
		//sort result by speed DESC and then by type ASC
		foreach ($result as $key => $row) {
			$hosts[$key] = $row['host'];
			$ports[$key] = $row['port'];
			$types[$key] = $row['type'];
			$speeds[$key] = $row['speed'];
		}
		array_multisort($speeds, SORT_ASC, $types, SORT_ASC, $result);
		$data = '';
		$count = count($result);
		foreach ($result as $item)
			$data .= $item['host'] . "," . $item['port'] . "," . $item['type']
					. "," . $item['speed'] . "\n";
		file_put_contents($input_file . '.tested', $data);
		echo_output(
				$count . ' proxy records sorted and saved at "' . $input_file
						. '.tested' . '"<br>', $is_console);
	} else
		die("Specify a proxy list to test, will you?");
} else
	die(
			"Operation canceled by user. The temporary work is saved at $err_file respectively $ok_file.\nNext time when running it will resume from the last check.");
?>
