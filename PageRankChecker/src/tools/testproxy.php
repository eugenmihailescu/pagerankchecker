<?php
/**
 * ----------------------------------------------------------------------------
 * This file is part of PageRankChecker.
 * 
 * PageRankChecker is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * PageRankChecker is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * PageRankChecker. If not, see <http://www.gnu.org/licenses/>.
 * 
 * ----------------------------------------------------------------------------
 * 
 * Git revision information:
 * 
 * @version:	0.2.1-alfa-0-g97f584c $
 * @commit:		819d3ee82127c1fb9fd8deff56771b2cd96690d0 $
 * @author:		Eugen Mihailescu <eugenmihailescux@gmail.com> $
 * @date:		Fri Jan 31 22:26:06 2014 +0100 $
 * @file:		testproxy.php $
 * 
 * @id:	testproxy.php | Fri Jan 31 22:26:06 2014 +0100 | Eugen Mihailescu  $
 * 
 */
?>

<!-- This is just a test page that you can use it to test your proxy -->
<form name="test_proxy" method="post" >
<table>
<tr><td>Host:</td><td><input type="text" name="host"/></td></tr>
<tr><td>Host:</td><td><input type="text" name="port"/></td></tr>
<tr><td>Type:</td><td><select name="type">
<option value=<?php echo CURLPROXY_HTTP; ?>>http</option>
<option value=<?php echo CURLPROXY_SOCKS4; ?>>socks4</option>
<option value=<?php echo CURLPROXY_SOCKS5; ?>>socks5</option>
</select></td></tr>
<tr><td colspan=2><input type="submit" name="submit" value="Test"/></td></tr>
</table>
</form>
<?php

include_once '../pagerank-functions.php';
if (isset($_POST['type']) && isset($_POST['host']) && isset($_POST['port'])) {

	$test = testProxy($_POST['host'], $_POST['port'], $_POST['type']);
	if ($test[0] == 0)
		$status = $test[1];
	else
		$status = "error ($test[1] ; code: $test[0])";
	echo 'The test finished with ' . $status . '. Total elapsed time '
			. sprintf('%.1f', 1e3 * $test[3]) . " ms";
}
?>
