<?php
/**
 * ----------------------------------------------------------------------------
 * This file is part of PageRankChecker.
 * 
 * PageRankChecker is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * PageRankChecker is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * PageRankChecker. If not, see <http://www.gnu.org/licenses/>.
 * 
 * ----------------------------------------------------------------------------
 * 
 * Git revision information:
 * 
 * @version:	0.2.1-alfa-0-g97f584c $
 * @commit:		819d3ee82127c1fb9fd8deff56771b2cd96690d0 $
 * @author:		Eugen Mihailescu <eugenmihailescux@gmail.com> $
 * @date:		Fri Jan 31 22:26:06 2014 +0100 $
 * @file:		crawl_proxies.php $
 * 
 * @id:	crawl_proxies.php | Fri Jan 31 22:26:06 2014 +0100 | Eugen Mihailescu  $
 * 
 */
?>

<?php
/**
 * Start the crawling process and save the list to a CSV file
 * @see For parallel processing : http://phplens.com/phpeverywhere/?q=node/view/254 
 */
include_once '../pagerank-functions.php';
foreach (glob("proxy_crawlers/*.php") as $filename) {
	include $filename;
}

/**
 * To launch it from OS console: php crawl_proxies.php -f../res/proxy_list.csv -c
 * where	f=output CSV filepath
 * 		 	c=I'm running at console so please echo like console-friendly
 */
$options = getopt("c::f::");
$is_console = isset($options['c']);
if (isset($options['f']))
	$output_file = $options['f'];
else if (isset($_GET['f'])) {
	$output_file = $_GET['f'];
}

/**
 * This just output the text below. It is used many 
 * times so it's nice to wrap that code in a function. 
 */
function echo_crawling($crawler) {
	global $is_console;
	echo_output("Crawling $crawler...<br>", $is_console);
}

function array_merge_unique($arrays) {
	$result = array();
	foreach ($arrays as $array) {
		$array = (array) $array;
		foreach ($array as $value) {
			if (array_search($value, $result) === false)
				$result[] = $value;
		}
	}
	return $result;
}

set_time_limit(0);
echo_crawling('getproxy.jp');
$a1 = proxyCrawler_getproxy();
echo_crawling('letushide.com');
$a2 = proxyCrawler_letushide();
echo_crawling('mrhinkydink.com');
$a3 = proxyCrawler_mrhinkydink();
echo_crawling('proxefr.blogspot.se');
$a4 = proxyCrawler_proxefr();

$proxies = array_merge_unique(array($a1, $a2, $a3, $a4));

$output = '';
$count = 0;
foreach ($proxies as $proxy) {
	if (count($proxy) > 0) {
		$output .= sprintf("%s,%s,%s,%s\r\n", $proxy['host'], $proxy['port'],
				$proxy['type'], $proxy['speed']);
		$count++;
	}
}

if (isset($output_file) && $output_file != '') {
	file_put_contents($output_file, $output);
	echo_output("$count proxies crawled and saved at $output_file<br>",
			$is_console);
}
?>
