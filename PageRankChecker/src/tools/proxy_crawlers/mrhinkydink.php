<?php
/**
 * ----------------------------------------------------------------------------
 * This file is part of PageRankChecker.
 * 
 * PageRankChecker is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * PageRankChecker is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * PageRankChecker. If not, see <http://www.gnu.org/licenses/>.
 * 
 * ----------------------------------------------------------------------------
 * 
 * Git revision information:
 * 
 * @version:	0.2.1-alfa-0-g97f584c $
 * @commit:		819d3ee82127c1fb9fd8deff56771b2cd96690d0 $
 * @author:		Eugen Mihailescu <eugenmihailescux@gmail.com> $
 * @date:		Fri Jan 31 22:26:06 2014 +0100 $
 * @file:		mrhinkydink.php $
 * 
 * @id:	mrhinkydink.php | Fri Jan 31 22:26:06 2014 +0100 | Eugen Mihailescu  $
 * 
 */
?>

<?php
include_once '../pagerank-functions.php';

/**
 * Crawl the page(s) below and get all the proxy info
 * @return array of proxy: host,port,type,speed/reliability
 */
function proxyCrawler_mrhinkydink() {
	$pageno = 1;
	$proxyUrl = "http://www.mrhinkydink.com/proxies%s.htm";
	$err = false;
	$proxies = array();

	while ($pageno < 11) {
		if ($pageno == 1)
			$url = sprintf($proxyUrl, "");
		else
			$url = sprintf($proxyUrl, $pageno);
		$ch = getCurlObject($url, 'localhost:9050', CURLPROXY_SOCKS5);
		switchIp('localhost:9051');
		$html = curl_exec($ch);

		$err = curl_errno($ch) != 0;

		curl_close($ch);

		if (!$err) {
			$dom = new DOMDocument();
			@$dom->loadHTML($html);
			$xpath = new DOMXPath($dom);
			$urlNodes = $xpath->query('//tr[@class="text"]');
			if (!is_null($urlNodes)) {
				foreach ($urlNodes as $element) {
					$proxy = array();
					$col = 0;
					$nodes = $element->childNodes;
					foreach ($nodes as $node) {
						switch ($col) {
						case 0:
							$proxy['host'] = str_replace("*", "",
									$node->nodeValue);
						case 2:
							$proxy['port'] = $node->nodeValue;
						case 4:
							$proxy['type'] = $node->nodeValue;
						case 6:
							$proxy['speed'] = $node->nodeValue;
						}
						$col++;
					}
					if (count($proxy) > 0 && hasIpAddress($proxy['host'])
							&& !array_search($proxy, $proxies))
						$proxies[count($proxies)] = $proxy;
				}
			}
		}
		$pageno++;
	}
	return $proxies;
}
?>
