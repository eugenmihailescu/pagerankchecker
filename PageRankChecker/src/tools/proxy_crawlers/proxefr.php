<?php
/**
 * ----------------------------------------------------------------------------
 * This file is part of PageRankChecker.
 * 
 * PageRankChecker is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * PageRankChecker is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * PageRankChecker. If not, see <http://www.gnu.org/licenses/>.
 * 
 * ----------------------------------------------------------------------------
 * 
 * Git revision information:
 * 
 * @version:	0.2.1-alfa-0-g97f584c $
 * @commit:		819d3ee82127c1fb9fd8deff56771b2cd96690d0 $
 * @author:		Eugen Mihailescu <eugenmihailescux@gmail.com> $
 * @date:		Fri Jan 31 22:26:06 2014 +0100 $
 * @file:		proxefr.php $
 * 
 * @id:	proxefr.php | Fri Jan 31 22:26:06 2014 +0100 | Eugen Mihailescu  $
 * 
 */

?>

<?php

include_once '../pagerank-functions.php';

/**
 * Crawl the page(s) below and get all the proxy info
 * @return array of proxy: host,port,type,speed/reliability
 */
function proxyCrawler_proxefr() {

	$proxyUrl = "http://proxefr.blogspot.se/";
	$err = false;
	$proxies = array();
	$ch = getCurlObject($proxyUrl, 'localhost:9050', CURLPROXY_SOCKS5);
	switchIp('localhost:9051');
	$html = curl_exec($ch);
	$err = curl_errno($ch) != 0;
	curl_close($ch);

	if (!$err) {
		$dom = new DOMDocument();
		@$dom->loadHTML($html);
		$xpath = new DOMXPath($dom);
		$urlNodes = $xpath->query('//textarea');

		if (!is_null($urlNodes)) {
			foreach ($urlNodes as $element) {
				$proxy = array();
				$nodes = $element->childNodes;
				foreach ($nodes as $node) {
					$lines = explode("\n", $node->nodeValue);

					foreach ($lines as $line)
						if (hasIpAddress($line)) {
							$data = explode(":", explode(" ", $line)[0]);
							$proxy['host'] = str_replace("*", "", $data[0]);
							$proxy['port'] = $data[1];
							$proxy['type'] = null;
							$proxy['speed'] = null;

							if (!array_search($proxy, $proxies))
								$proxies[count($proxies)] = $proxy;
						}
				}
			}
		}
	}
	return $proxies;
}

?>
